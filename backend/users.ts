export class User {
    constructor(public email: string,
                public name: string,
                private password: string ) {}

    matches(another : User ) : boolean {
        return another !== undefined &&
         another.email === this.email &&
         another.password === this.password
    }
}

export const users : {[key:string] : User} = {
    "rosana@gmail.com" : new User('rosana@gmail.com', 'Rosana', 'rosana1'),
    "sonia@gmail.com" : new User('sonia@gmail.com', 'Sonia', 'sonia123')
}
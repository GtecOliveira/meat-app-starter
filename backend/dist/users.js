"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User(email, name, password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
    User.prototype.matches = function (another) {
        return another !== undefined &&
            another.email === this.email &&
            another.password === this.password;
    };
    return User;
}());
exports.User = User;
exports.users = {
    "rosana@gmail.com": new User('rosana@gmail.com', 'Rosana', 'rosana1'),
    "sonia@gmail.com": new User('sonia@gmail.com', 'Sonia', 'sonia123')
};
//# sourceMappingURL=users.js.map